# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### Project 2 Requirements:

_Deliverables:_

1. Server-side validation  
2. Compile servlet files  
3. Provide Bitbucket read-only access  
4. Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4368/src/master/  
    b) https://bitbucket.org/rv17h/lis4368/src/master/p2/  
    

README.md file should include the following items:
    * Screenshots of pre-, post-valid user form entry, as well as MySQL customer table entry (see screenshots below)  
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
    
Assignment Screenshots and Links:

Valid User Form Entry (customerform.jsp) | Passed Validation (thanks.jsp)
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

Display Data (customers.jsp) | Modify Form (modify.jsp)
:-------------------------:|:-------------------------:
![](img/3.png)  |  ![](img/4.png)

Modified Data (customers.jsp) | Delete Warning (customers.jsp)
:-------------------------:|:-------------------------:
![](img/5.png)  |  ![](img/6.png)

Associated Database Changes (Select, Insert, Update, Delete) | 
:-------------------------:|:-------------------------:
![](img/7.png)  |  