# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### Assignment 3 Requirements:

_Deliverables:_

1. Entity Relationship Diagram
2. Include data (at least 10 records in each table)
3. Provide Bitbucket read-only access
    * docs folder: a3.mwb and a3.sql  
    * img folder: a3.png (export a3.mwb file as a3.png)
    * README.md (MUST display a3.png ERD)
4. Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4368/src/master/  
    b) https://bitbucket.org/rv17h/lis4368/src/master/a3/
    

README.md file should include the following items:
    * Screenshot of ERD that links to the image
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 

    
Assignment Screenshots and Links:

*Screenshot A3 ERD*:
![A3 ERD](img/a3.png)

*A3 docs: a3.mwb and a3.sql*:  

[A3 MWB File](docs/a3.mwb)  

[A3 SQL File](docs/a3.sql)
