# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### LIS4368 Requirements:

_Course Work Links:_

1. [A1 README.md](https://bitbucket.org/rv17h/lis4368/src/master/a1/README.md)
	* Install JDK
	* Install Tomcat
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorial (bitbucketstationlocations)
	* Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/rv17h/lis4368/src/master/a2/README.md)
	* MySQL Client Login Using AMPPS
	* Finish Tomcat tutorial
	* Assessment links
	* Provide screenshots of web app and query results
3. [A3 README.md](https://bitbucket.org/rv17h/lis4368/src/master/a3/README.md)
	* Entity Relationship Diagram
	* Include data (at least 10 records in each table)
	* Provide Bitbucket read-only access
4. [A4 README.md](https://bitbucket.org/rv17h/lis4368/src/master/a4/README.md)
	* Server-side validation
	* Compile servlet files 
5. [A5 README.md](https://bitbucket.org/rv17h/lis4368/src/master/a5/README.md)
	* [Placeholder]    
	* [Placeholder]  
	* [Placeholder] 
6. [P1 README.md](https://bitbucket.org/rv17h/lis4368/src/master/p1/README.md)
	* Suitably modify meta tags
	* Change title, navigation links, and header tags appropriately
	* Add form controls to match attributes of customer entity
	* Add jQuery validation and regular expressions-- as per the entity attribute requirements
7. [P2 README.md](https://bitbucket.org/rv17h/lis4368/src/master/p2/README.md)
	* [Placeholder] 
	* [Placeholder] 