# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### Assignment 4 Requirements:

_Deliverables:_

1. Server-side validation  
2. Compile servlet files  
3. Provide Bitbucket read-only access  
4. Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4368/src/master/  
    b) https://bitbucket.org/rv17h/lis4368/src/master/a4/
    

README.md file should include the following items:
    * Screenshot of failed and passed validation
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
    
Assignment Screenshots and Links:

Failed Validation | Passed Validation
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)