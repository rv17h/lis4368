-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema root
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `root` ;

-- -----------------------------------------------------
-- Schema root
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `root` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `root` ;

-- -----------------------------------------------------
-- Table `root`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`category` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`category` (
  `cat_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_type` ENUM('salary', 'bonus', 'pension', 'social security', 'unemployment', 'disability', 'royalty', 'interest', 'dividend', 'tax return', 'gift', 'child support', 'alimony', 'award', 'grant', 'scholarship', 'inheritance', 'housing', 'food', 'transportation', 'charity', 'investment', 'insurance', 'clothing', 'saving', 'health', 'personal', 'recreation', 'debt', 'school', 'childcare', 'misc') NOT NULL,
  `cat_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cat_id`),
  UNIQUE INDEX `cat_type_UNIQUE` (`cat_type` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`user` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`user` (
  `usr_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `usr_fname` VARCHAR(15) NOT NULL,
  `usr_lname` VARCHAR(30) NOT NULL,
  `usr_street` VARCHAR(30) NOT NULL,
  `usr_city` VARCHAR(30) NOT NULL,
  `usr_state` CHAR(2) NOT NULL,
  `usr_zip` INT UNSIGNED ZEROFILL NOT NULL,
  `usr_phone` BIGINT UNSIGNED NOT NULL,
  `usr_email` VARCHAR(100) NULL,
  `usr_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`usr_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`institution`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`institution` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`institution` (
  `ins_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ins_name` VARCHAR(30) NOT NULL,
  `ins_street` VARCHAR(30) NOT NULL,
  `ins_city` VARCHAR(30) NOT NULL,
  `ins_state` CHAR(2) NOT NULL,
  `ins_zip` INT UNSIGNED ZEROFILL NOT NULL,
  `ins_phone` BIGINT UNSIGNED NOT NULL,
  `ins_email` VARCHAR(100) NOT NULL,
  `ins_url` VARCHAR(100) NOT NULL,
  `ins_contact` VARCHAR(45) NULL,
  `ins_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ins_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`account` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`account` (
  `act_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_type` VARCHAR(20) NOT NULL,
  `act_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`act_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`source`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`source` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`source` (
  `src_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `usr_id` TINYINT UNSIGNED NOT NULL,
  `ins_id` TINYINT UNSIGNED NOT NULL,
  `act_id` TINYINT UNSIGNED NOT NULL,
  `src_start_date` DATE NOT NULL,
  `src_end_date` DATE NULL,
  `src_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`src_id`),
  INDEX `fk_source_user1_idx` (`usr_id` ASC),
  INDEX `fk_source_institution1_idx` (`ins_id` ASC),
  INDEX `fk_source_account1_idx` (`act_id` ASC),
  UNIQUE INDEX `ux_usr_ins_act` (`usr_id` ASC, `act_id` ASC, `ins_id` ASC),
  CONSTRAINT `fk_source_user1`
    FOREIGN KEY (`usr_id`)
    REFERENCES `root`.`user` (`usr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_source_institution1`
    FOREIGN KEY (`ins_id`)
    REFERENCES `root`.`institution` (`ins_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_source_account1`
    FOREIGN KEY (`act_id`)
    REFERENCES `root`.`account` (`act_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`transaction` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`transaction` (
  `trn_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `src_id` SMALLINT UNSIGNED NOT NULL,
  `cat_id` TINYINT UNSIGNED NOT NULL,
  `trn_type` ENUM('credit', 'debit') NOT NULL,
  `trn_method` VARCHAR(15) NOT NULL,
  `trn_amt` DECIMAL(7,2) UNSIGNED NOT NULL,
  `trn_date` DATETIME NOT NULL,
  `trn_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`trn_id`),
  INDEX `fk_transaction_source_idx` (`src_id` ASC),
  INDEX `fk_transaction_category1_idx` (`cat_id` ASC),
  CONSTRAINT `fk_transaction_source`
    FOREIGN KEY (`src_id`)
    REFERENCES `root`.`source` (`src_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_category1`
    FOREIGN KEY (`cat_id`)
    REFERENCES `root`.`category` (`cat_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED ZEROFILL NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_total_sales` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `root`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `root`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `root`.`category`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'housing', NULL);
INSERT INTO `root`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'food', NULL);
INSERT INTO `root`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'transportation', NULL);
INSERT INTO `root`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'insurance', NULL);
INSERT INTO `root`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'personal', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`user` (`usr_id`, `usr_fname`, `usr_lname`, `usr_street`, `usr_city`, `usr_state`, `usr_zip`, `usr_phone`, `usr_email`, `usr_notes`) VALUES (DEFAULT, 'John', 'Doe', '123 Main St.', 'Des Moines', 'IA', 645320123, 5713247659, 'jdoe@aol.com', NULL);
INSERT INTO `root`.`user` (`usr_id`, `usr_fname`, `usr_lname`, `usr_street`, `usr_city`, `usr_state`, `usr_zip`, `usr_phone`, `usr_email`, `usr_notes`) VALUES (DEFAULT, 'Jane', 'Parker', '85378 Elm St.', 'Detroit', 'MI', 482347821, 3136579012, 'jparker@yahoo.com', NULL);
INSERT INTO `root`.`user` (`usr_id`, `usr_fname`, `usr_lname`, `usr_street`, `usr_city`, `usr_state`, `usr_zip`, `usr_phone`, `usr_email`, `usr_notes`) VALUES (DEFAULT, 'Bonnie', 'Val Kyries', '39167 Alabama Ave', 'Chicago', 'IL', 500147863, 5016749831, 'bvalkyries@att.net', NULL);
INSERT INTO `root`.`user` (`usr_id`, `usr_fname`, `usr_lname`, `usr_street`, `usr_city`, `usr_state`, `usr_zip`, `usr_phone`, `usr_email`, `usr_notes`) VALUES (DEFAULT, 'Billy', 'Joel', '503 Foothill Blvd', 'Sacramento', 'CA', 908675301, 9086753018, 'bjoel@comcast.net', NULL);
INSERT INTO `root`.`user` (`usr_id`, `usr_fname`, `usr_lname`, `usr_street`, `usr_city`, `usr_state`, `usr_zip`, `usr_phone`, `usr_email`, `usr_notes`) VALUES (DEFAULT, 'Johnny', 'Depp', '123 4th Ave', 'New York', 'NY', 200256743, 1001784526, 'jdepp@horizon.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`institution`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`institution` (`ins_id`, `ins_name`, `ins_street`, `ins_city`, `ins_state`, `ins_zip`, `ins_phone`, `ins_email`, `ins_url`, `ins_contact`, `ins_notes`) VALUES (DEFAULT, 'Regions', '2320 Tennessee St', 'Tallahassee', 'FL', 323047634, 8005551234, 'bob@regions.com', 'http://www.regions.com', 'Bob Flounder', NULL);
INSERT INTO `root`.`institution` (`ins_id`, `ins_name`, `ins_street`, `ins_city`, `ins_state`, `ins_zip`, `ins_phone`, `ins_email`, `ins_url`, `ins_contact`, `ins_notes`) VALUES (DEFAULT, 'Wells Fargo', '87413 Pensacola', 'Tallahassee', 'FL', 323021251, 8005554321, 'chiggins@wellsfargo.com', 'http://www.wellsfargo.com', 'Cheryl Higgins', NULL);
INSERT INTO `root`.`institution` (`ins_id`, `ins_name`, `ins_street`, `ins_city`, `ins_state`, `ins_zip`, `ins_phone`, `ins_email`, `ins_url`, `ins_contact`, `ins_notes`) VALUES (DEFAULT, 'SunTrust', '178 South Ave', 'Atlanta', 'GA', 303538974, 8005556789, 'contact@suntrust.com', 'http://www.suntrust.com', 'Louise McFlaggen', NULL);
INSERT INTO `root`.`institution` (`ins_id`, `ins_name`, `ins_street`, `ins_city`, `ins_state`, `ins_zip`, `ins_phone`, `ins_email`, `ins_url`, `ins_contact`, `ins_notes`) VALUES (DEFAULT, 'Great Lakes Loan Group', '2401 International Ln', 'Madison', 'WI', 537043121, 8002364300, 'info@gllg.com', 'http://www.gllg.com', 'John Redland', NULL);
INSERT INTO `root`.`institution` (`ins_id`, `ins_name`, `ins_street`, `ins_city`, `ins_state`, `ins_zip`, `ins_phone`, `ins_email`, `ins_url`, `ins_contact`, `ins_notes`) VALUES (DEFAULT, 'TIAA-CREF', '730 3rd Ave', 'NY', 'NY', 100172093, 8007191185, 'info@tiaacreff.com', 'http://www.tiaacreff.com', 'Peter Bane', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`account`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`account` (`act_id`, `act_type`, `act_notes`) VALUES (DEFAULT, 'checking', NULL);
INSERT INTO `root`.`account` (`act_id`, `act_type`, `act_notes`) VALUES (DEFAULT, 'savings', NULL);
INSERT INTO `root`.`account` (`act_id`, `act_type`, `act_notes`) VALUES (DEFAULT, 'mortgage', NULL);
INSERT INTO `root`.`account` (`act_id`, `act_type`, `act_notes`) VALUES (DEFAULT, 'school loan', NULL);
INSERT INTO `root`.`account` (`act_id`, `act_type`, `act_notes`) VALUES (DEFAULT, 'investment', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`source`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`source` (`src_id`, `usr_id`, `ins_id`, `act_id`, `src_start_date`, `src_end_date`, `src_notes`) VALUES (DEFAULT, 1, 1, 1, '2001-03-31', NULL, NULL);
INSERT INTO `root`.`source` (`src_id`, `usr_id`, `ins_id`, `act_id`, `src_start_date`, `src_end_date`, `src_notes`) VALUES (DEFAULT, 1, 1, 2, '2001-05-07', NULL, NULL);
INSERT INTO `root`.`source` (`src_id`, `usr_id`, `ins_id`, `act_id`, `src_start_date`, `src_end_date`, `src_notes`) VALUES (DEFAULT, 2, 1, 3, '2003-01-02', '2010-07-11', NULL);
INSERT INTO `root`.`source` (`src_id`, `usr_id`, `ins_id`, `act_id`, `src_start_date`, `src_end_date`, `src_notes`) VALUES (DEFAULT, 2, 2, 4, '2005-11-19', NULL, NULL);
INSERT INTO `root`.`source` (`src_id`, `usr_id`, `ins_id`, `act_id`, `src_start_date`, `src_end_date`, `src_notes`) VALUES (DEFAULT, 3, 2, 5, '2002-09-09', '2011-02-24', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Paws and Claws', '123 Paws St.', 'Tallahassee', 'FL', 32303, 8501234567, 'support@pawsandclaws.com', 'pawsandclaws.com', 500000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'K9 Central', '345 Canine Cir', 'Tallahassee', 'FL', 32304, 8503456789, 'support@k9central.net', 'k9central.net', 300000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pitbull City', '567 Pitbull Lane', 'Tallahassee', 'FL', 32305, 8505678901, 'help@pitbullcity.com', 'pitbullcity.com', 100000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetSmart', '890 Smart Ave', 'Tallahassee', 'FL', 32306, 8507890123, 'support@petsmart.com', 'petsmart.com', 1000000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Petco', '123 Petco Loop', 'Tallahassee', 'FL', 32307, 8501235678, 'contact@petco.com', 'petco.com', 1000000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Chewy', '345 Chewy St.', 'Tallahassee', 'FL', 32308, 8503457890, 'support@chewy.com', 'chewy.com', 2000000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Supermarket', '567 Super Lane', 'Tallahassee', 'FL', 32309, 8505671234, 'support@petsupermarket.com', 'petsupermarket.com', 1500000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Boutique', '890 Boutique Cir', 'Tallahassee', 'FL', 32310, 8507890123, 'contact@petboutique.com', 'petboutique.com', 200000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Healthy Pets', '123 Healthy Drive', 'Tallahassee', 'FL', 32311, 8501237890, 'support@healthypets.com', 'healthypets.com', 100000, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Place', '345 Pet Place', 'Tallahassee', 'FL', 32312, 8503451234, 'help@petplace.com', 'petplace.com', 150000, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Andrew', 'Barnes', '123 Tennessee St.', 'Tallahassee', 'FL', 32303, 8508881234, 'abarnes@gmail.com', 139.52, 987.61, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bob', 'Cullen', '345 Pensacola St.', 'Tallahassee', 'FL', 32304, 8509995678, 'bcullen@yahoo.com', 49.87, 1356.89, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Cristina', 'Delmonico', '567 Fifth Ave', 'Tallahassee', 'FL', 32305, 8503359120, 'cdelmonico@gmail.com', 0.00, 600.00, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'David', 'Evans', '789 Monroe St.', 'Tallahassee', 'FL', 32305, 8505557890, 'devans@gmail.com', 20.35, 756.89, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Eddie', 'Franklin', '22 S Adams St.', 'Tallahassee', 'FL', 32304, 8507721234, 'efranklin@gmail.com', 1247.91, 1501.77, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Freddie', 'Gutierrez', '2150 Mission Rd.', 'Tallahassee', 'FL', 32303, 7725672132, 'fgutierrez@gmail.com', 300.00, 950.87, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Gavin', 'Holiday', '123 Hartsfield Rd.', 'Tallahassee', 'FL', 32303, 5612225678, 'gholiday@comcast.net', 400.00, 2237.67, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Helen', 'Ingram', '456 Old Bainbridge Rd.', 'Tallahassee', 'FL', 32303, 8509992525, 'hingram@gmail.com', 0.00, 900.00, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Ivan', 'Jenkins', '22 Tharpe St.', 'Tallahassee', 'FL', 32303, 7728903476, 'ijenkins@outlook.com', 22.36, 850.46, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jeremy', 'Killings', '670 Apalachee Pkwy', 'Tallahassee', 'FL', 32312, 8506313309, 'jkillings@icloud.com', 0.00, 700.81, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 5, 'Doberman', 'm', 300, 500, 6, 'black/brown', '2018-01-07', 'y', 'n', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, NULL, 'Chihuahua', 'f', 200, 400, 12, 'tan', NULL, 'n', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 5, 'Boston Terrier', 'f', 100, 300, 18, 'white', '2017-02-02', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'American Pitbull Terrier', 'm', 500, 700, 6, 'blue', '2018-11-05', 'y', 'n', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, NULL, 'Poodle', 'f', 300, 500, 12, 'black', NULL, 'n', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 5, 'Golden Retriever', 'm', 200, 400, 6, 'golden', '2018-03-03', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 4, 'Labrador', 'f', 200, 400, 6, 'chocolate', '2018-07-20', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 7, 'Staffordshire Terrier', 'm', 300, 600, 6, 'black', '2018-06-06', 'y', 'n', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 4, 'American Bully', 'm', 500, 700, 6, 'white', '2018-05-05', 'y', 'n', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 2, 'Black Russian Terrier', 'm', 400, 600, 6, 'black', '2018-04-04', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 9, 'Afghan Hound', 'f', 300, 500, 18, 'grey', '2018-08-08', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 10, 'Argentine Dogo', 'm', 800, 1200, 6, 'white', '2018-09-28', 'y', 'n', NULL);

COMMIT;

