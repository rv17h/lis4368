# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### Assignment 1 Requirements:

_Three Parts:_

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installations
3. Questions
4. Bitbucket repo links:  

    a) https://bitbucket.org/rv17h/lis4368/src/master/  
    
    b) https://bitbucket.org/rv17h/bitbucketstationlocations/src/master/

README.md file should include the following items:
    * Screenshot of a1 application running
    * git commands w/ short descriptions
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
> Git commands w/ short descriptions:"

    1. git init - creates a new git repository
    2. git status - get the status of local repository or file
    3. git add - tells git to track new files
    4. git commit - takes staging changes and commits them to project history
    5. git push - pushes commited changes to repository
    6. git pull - pulls changes from remote repository into local repository
    7. git clone - clones a repository
    
Assignment Screenshots:

Hello.java

![Hello.java](img/1.png)

localhost:9999             |  localhost:9999/lis4368
:-------------------------:|:-------------------------:
![localhost:9999 ](img/2.png)  |  ![localhost:9999/lis4368](img/3.png)
    
