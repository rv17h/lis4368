# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### Assignment 5 Requirements:

_Deliverables:_

1. Server-side validation  
2. Compile servlet files  
3. Provide Bitbucket read-only access  
4. Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4368/src/master/  
    b) https://bitbucket.org/rv17h/lis4368/src/master/a5/  
    

README.md file should include the following items:
    * Screenshots of pre-, post-valid user form entry, as well as MySQL customer table entry (see screenshots below)  
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
    
Assignment Screenshots and Links:

Valid User Form Entry (customerform.jsp) | Passed Validation (thanks.jsp)
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

Associated Database Entry | 
:-------------------------:|:-------------------------:
![](img/3.png)  |  