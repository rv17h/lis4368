# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### Project 1 Requirements:

_Deliverables:_

1. All input fields, except Notes are required
2. Use min/max jQuery validation
3. Use regexp to only allow appropriate characters for each control
    * cus_fname, cus_lname,cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email, cus_balance,cus_total_sales, cus_notes  
    * fname,lname: provided  
    * street, city:  
        no more than 30 characters  
        Street: must only contain letters, numbers, commas, hyphens, or periods  
        City: can only contain letters, numbers, hyphens, and space character (29 Palms)  
    * state:  
        must be 2 characters  
        must only contain letters  
    * zip:  
        must be between 5 and 9 characters, inclusive 
        must only contain numbers  
    * phone:  
        must be 10 characters, including area code
        must only contain numbers  
    * email:  
        provided (also, see below: Lesson 7)  
    * balance, total_sales:  
        no more than 6 digits, including decimal point  
        can only contain numbers, and decimal point (if used)
4. After testing jQuery validation, use HTML5 property to limit the number of characters for each control
5. Research what the following validation code does:  
    * valid: 'fa fa-check',
    * invalid: 'fa fa-check',
    * validating: 'fa fa-refresh'        
6. Bitbucket repo links:  
    a) https://bitbucket.org/rv17h/lis4368/src/master/  
    b) https://bitbucket.org/rv17h/lis4368/src/master/p1/
    

README.md file should include the following items:
    * Screenshot of ERD that links to the image
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 

    
Assignment Screenshots:

LIS4368 Portal (Main/Splash Page) | LIS Portal (Slide 2)
:-------------------------:|:-------------------------:
![](img/1.png)  |  ![](img/2.png)

Failed Validation | Passed Validation
:-------------------------:|:-------------------------:
![](img/3.png)  |  ![](img/4.png)
