# LIS4368 - Advanced Web Applications Development

## Rene Valencia

### Assignment 2 Requirements:

_All Parts:_

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installations
3. Questions
4. Bitbucket repo:  

    a) https://bitbucket.org/rv17h/lis4368/src/master/  
    b) http://localhost:9999/hello (displays directory, needs index.html)  
    c) http://localhost:9999/hello/HelloHome.html (Renamed"HelloHome.html" to "index.html")  
    d) http://localhost:9999/hello/sayhello(invokes HelloServlet) Note: /sayhello maps to HelloServlet.class (changed web.xml file)  
    e) http://localhost:9999/hello/querybook.html  
    f) http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)  

README.md file should include the following items:
    * Screenshot of application running
    * git commands w/ short descriptions
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
> Git commands w/ short descriptions:"

    1. git init - creates a new git repository
    2. git status - get the status of local repository or file
    3. git add - tells git to track new files
    4. git commit - takes staging changes and commits them to project history
    5. git push - pushes commited changes to repository
    6. git pull - pulls changes from remote repository into local repository
    7. git clone - clones a repository
    
Assignment Screenshots:

localhost:9999/hello       |  localhost:9999/sayhello
:-------------------------:|:-------------------------:
![localhost:9999/hello](img/1.png)  |  ![localhost:9999/sayhello](img/2.png)

localhost:9999/querybook.html |  localhost:9999/sayhi
:-------------------------:|:-------------------------:
![localhost:9999/querybook.html](img/3.png)  |  ![localhost:9999/sayhi](img/4.png)

localhost:9999/querybook.html (selected item) |  query results
:-------------------------:|:-------------------------:
![localhost:9999/querybook.html (selected item)](img/5.png)  |  ![query results](img/6.png)
    
