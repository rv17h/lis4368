import java.util.Scanner;

public class CopyArray {
  public static void main(String [] args) {
    System.out.print("Program Requirements:"
    + "\n1. Create a five element string array (str1)."
    + "\n2. Create a second string array (str2) based upon the length of str1 array."
    + "\n3. Copy str1 array elements into str2."
    + "\n4. Print elements of both arrays using Java's *enhanced* for loop.\n\n");
    
    String str1[] = {"C++", "C", "Java", "Python", "JSON"};
    String str2[] = new String[str1.length];

    for (int i = 0; i < str1.length; i++){
      str2[i] = str1[i];
    }
    
    System.out.println("Printing str1 array:");
    for (String s: str1){
      System.out.println(s);
    }

    System.out.println("\nPrinting str2 array:");
    for (String s: str2){
      System.out.println(s);
    }
  }
}  