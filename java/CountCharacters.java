import java.util.Scanner;

public class CountCharacters {
  public static void main(String[] args) {
    System.out.println("Program counts number and types of characters: that is, letters, spaces, numbers, and other characters."
    + "\nHint: You may find the following methods helpful: isLetter(), isDigit(), isSpaceChar()."
    + "\nAdditionally, you could add the functionality of testing for upper vs lower case characters.\n\n");

    Scanner sc = new Scanner(System.in);
    System.out.print("Please enter string: ");
    
    String test = sc.nextLine();
    count(test);
    sc.close();
  }

	public static void count(String x) {
    char[] ch = x.toCharArray();
    int letter = 0;
    int space = 0;
    int num = 0;
    int other = 0;

		for(int i = 0; i < x.length(); i++){
			if(Character.isLetter(ch[i])) {
				letter ++ ;
			}
			else if(Character.isDigit(ch[i])) {
				num ++ ;
			}
			else if(Character.isSpaceChar(ch[i])) {
				space ++ ;
			}
			else {
				other ++;
			}
    }
    
		System.out.println("\n\nYour string: \"" + x + "\" has the following number and types of characters:");
		System.out.println("letter(s): " + letter);
		System.out.println("space(s): " + space);
		System.out.println("number(s): " + num);
		System.out.println("other character(s): " + other);
	}
}