import java.util.Scanner;

public class NumberSwap
{
   public static void main(String args[])
   {
    int num1, num2, temp;
    Scanner sc = new Scanner(System.in);
    System.out.print("Program swaps two integers. \nNote: Program checks for integers and non-numeric values.");
    
    System.out.print("\n\nPlease enter first number: ");
    while (!sc.hasNextInt()) {
      System.out.print("Not a valid integer!");
      System.out.print("\n\nPlease try again. Enter first number: ");
      sc.next();
    }
    num1 = sc.nextInt();

    System.out.print("\nPlease enter second number: ");
    while (!sc.hasNextInt()) {
      System.out.print("Not a valid integer!");
      System.out.print("\n\nPlease try again. Enter second number: ");
      sc.next();
    }
    num2 = sc.nextInt();

    System.out.println("\nBefore Swapping\nnum1 = " + num1 + "\nnum2 = " + num2);
     
    temp = num1;
    num1 = num2;
    num2 = temp;
     
      System.out.println("\nAfter Swapping\nnum1 = " + num1 + "\nnum2 = " + num2);

    // System.out.print("\nBefore Swapping");
    // System.out.print("\nnum1 = " + num1);
    // System.out.print("\nnum2 = " + num2);
    // System.out.println();

    // System.out.print("\nAfter Swapping");
    // System.out.print("\nnum1 = " + num2);
    // System.out.print("\nnum2 = " + num1);
    // System.out.println();

    sc.close();
    }
  }