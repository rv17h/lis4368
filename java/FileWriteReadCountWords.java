import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
 
public class FileWriteReadCountWords {
	public static void main(String[] args) throws IOException {
 
		String fileName = "filecountwords.txt";
		try {
      System.out.println("Program captures user input, writes to and reads from same file, and counts number of words in file."
    + "\nHint: use hasNext() method to read number of words (tokens).\n\n");

      FileWriter file = new FileWriter(fileName);
      
      Scanner sc = new Scanner(System.in);
      System.out.print("Please enter text: ");

			file.write(sc.nextLine());
			file.close();
      System.out.println("Saved to file " + "\"" + fileName + "\"");

			int count = 0;
			Scanner sc2 = new Scanner(new File(fileName));
 
			// Count words
			while (sc2.hasNext()) {
				count++;
				sc2.next();
			}
 
      System.out.println("Number of words: " + count);
      
			sc.close();
 
		} catch (FileNotFoundException e) {
			System.out.println("The given file " + fileName + "  not found ");
		}
 
	}
}