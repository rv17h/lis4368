import java.util.Scanner;

public class Ascii {
  public static void main(String [] args) {
    char c;
    int asciiValue;
    int i;
    int input;

    System.out.print("Printing characters A-Z as ASCII values:");
    for(c = 'A'; c <= 'Z'; ++c) {
      asciiValue = (int)c;
      System.out.print("\nCharacter " + c + " has ascii value " + asciiValue);
    }

    System.out.print("\n\nPrinting ASCII values 48-122 as characters:");
    for(i = 48; i <= 122; ++i) {
      asciiValue = (int)c;
      System.out.print("\nASCII value " + i + " has character value " + Character.toString((char) i));
    }

    Scanner sc = new Scanner(System.in);

    System.out.print("\n\nAllowing user ASCII value input:");

    while (true) {
      System.out.print("\nPlease enter ASCII value (32 - 127): ");
      if (sc.hasNextInt()) {
        input = sc.nextInt();
        if (input >= 32 && input <= 127) {
          break;
        }
        else {
          System.out.print("ASCII value must be >= 32 and <= 127.\n");
        }
      }
      else {
        System.out.print("Invalid integer--ASCII value must be a number.\n");
        sc.next();
      }
    }

    System.out.print("\nASCII value " + input + " has character value " + Character.toString((char) input));
    System.out.println();
    sc.close();
  }
}  